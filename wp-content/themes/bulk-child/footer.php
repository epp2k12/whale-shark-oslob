<style type="text/css">
.footer_entry_1 {
	border:1px solid red;
}

td {
	padding:0px;
	margin:0px;
}

input {
	padding:0px;
	margin:0px;
}

ul {
    list-style:none;
    padding:0;
    margin:0;
}
label {
  display: block;
  text-indent: -15px;
  margin:0px;
  padding: 0px 15px;
  font-size:.9em;
}
input[type=checkbox] {
  width: 13px;
  height: 13px;
  padding: 0;
  margin:0;
  vertical-align: bottom;
  position: relative;
  top: -8px;
  *overflow: hidden;
}

.quick_easy_booking {
	overflow: auto;
}

.quick_easy_booking h3 {
	margin:0px;
	text-align:center;
}

#form_easybooking {
	overflow:auto;
}
#easy_book {
  color:#ffffff;
  background-color:#000000;
  width:80%;
  height:60px;
  font-size:1.4em;
  border-radius:10px;

}
#easy_book:hover{
  color:#fcff06;
}

.logo_section {
  background-color:#ffffff;
  margin:30px 0px;
}

.terms_section {
  background-color:#b5dcff;
  margin:10px 0px;
  padding:10px;
  text-align:center;
}

.title_news_section {
  background-color:#000000;
  color:#ffffff;
  padding:4px;
}

</style>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- wso_add01 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-6097410298968540"
     data-ad-slot="9596944147"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<div class="quick_easy_booking front_group">
<h3 class="pages_title_bars"><strong style="color:#ffffff">QUICK AND EASY BOOKING <br/>
<span style="font-size:.7em;color:#fdf12c;">For your customized tour, Select the <strong>number of days</strong> and the <strong>activities</strong></span>
</strong></h3>

<div class="col-xs-12 col-sm-5">
<form id="form_easybooking">
<strong style="color:#ff0000">JUST FILL UP FOR EASY BOOKING!</strong>
<input type="hidden" name="q_submitted" value="true">

  <table class="table table-condensed table_compute" id="table_easy_book" >
  <tr><td>
	Full Name
  </td><td>
  <input type="text" name="q_full_name" class="form-control" id="q_full_name" placeholder="Enter Full Name" required>
  </td></tr>
  <tr><td>
  Email
  </td><td>
  <input type="email" name="q_email" class="form-control" id="q_email" placeholder="Enter Best Email" required>
  </td></tr>

  <tr><td>
  Contact
  </td><td>
  <input type="text" name="q_contact" class="form-control" id="q_contact" placeholder="Enter Contact Number">
  </td></tr>

  <tr><td>
  Pick-up Date
  </td><td>
  <input type="text" name="q_pick_date" class="form-control" id="q_pick_date" placeholder="mm/dd/yyyy" required>
  </td></tr>
  <tr><td>
  Pick-up Place
  </td><td>
  <input type="text" name="q_pick_place" class="form-control" id="q_pick_place" placeholder="Hotel/Airport in Cebu" required>
  </td></tr>
  <tr><td><span class="impt_text">No. of Person(s)</td>
  <td>
    <select name="q_no_of_persons" class="form-control" id="q_no_of_persons" style="width:100%">
    </select>
  </td>
  </tr>
  <tr><td><span class="impt_text">No. of Day(s) Tour</td>
  <td>
    <select name="q_no_of_days" class="form-control" id="q_no_of_days" style="width:100%">
    </select>
  </td>
  </tr>
  <tr><td colspan=2>
	Additional Info<br/>
	<textarea class="noresize form-control" name="q_other_info" id="other_info"  rows="2" placeholder="Additional Info"></textarea>
  </td></tr>
  </table>
</div> <!-- col-xs-12 col-sm-4 -->

<div class="col-xs-12 col-sm-7">
<strong style="color:#ff0000">PLEASE CHOOSE ACTIVITIES HERE ...</strong>

<div class="row">
<div class="col-xs-12 col-sm-6">
<label><input type="checkbox" name="tour1" value="Oslob Whale" checked/> Oslob Whale</label>
<label><input type="checkbox" name="tour2" value="Kawasan Falls" /> Kawasan Falls</label>
<label><input type="checkbox" name="tour3" value="Kawasan Falls and Canyoneering"> Kawasan Canyoneering</label>
<label><input type="checkbox" name="tour4" value="Mactan Island Hopping"> Mactan Island Hopping</label>
<label><input type="checkbox" name="tour5" value="Tumalog Falls"> Tumalog Falls</label>
<label><input type="checkbox" name="tour6" value="Cebu City Tour"> Cebu City Tour</label>
</div>

<div class="col-xs-12 col-sm-6">
<label><input type="checkbox" name="tour7" value="Simala"> Simala</label>
<label><input type="checkbox" name="tour8" value="Bohol Country Side Tour"> Bohol Country Side Tour</label>
<label><input type="checkbox" name="tour9" value="Pescador Island Hopping"> Pescador Island Hopping</label>
<label><input type="checkbox" name="tour10" value="10,000 Roses"> 10,000 Roses</label>
<label><input type="checkbox" name="tour11" value="Malapascua Island"> Malapascua Island</label>
<label><input type="checkbox" name="tour12" value="Kalanggaman Island"> Kalanggaman Island</label>
</div>
</div> <!-- row -->

</div> <!-- col-xs-12 col-sm-8 -->
<div class="col-xs-12 submit_quick_button" style="margin:0px auto;text-align:center;">
<input  type="submit" value="SUBMIT BOOKING" id="easy_book">
</div>

</form> <!-- form_easybooking -->
</div> <!-- quick_easy_booking -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- wso_add01 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-6097410298968540"
     data-ad-slot="9596944147"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

<div class="news_customer clearfix">
<div class="col-sm-3 news_container">
<div class="news_customer_group">
<div class='title_news_section'>RECENT NEWS ...</div>
<?php
    $args=array(
    'cat' => 13,
    'posts_per_page'=> 1
    );
    $blogPosts = new WP_Query( $args );
      if( $blogPosts->have_posts() ) {
      //loop through related posts based on the tag
        while ( $blogPosts->have_posts() ) {

          $blogPosts->the_post();
            echo '<a href="'. get_the_permalink() .'" class="title_news">';
            the_title();
            echo '</a>';
            echo '<a href="'. get_the_permalink() .'">';
            the_post_thumbnail();
            echo '</a>';
            echo '<br/>';
            the_content();
        }
      }
    wp_reset_postdata();
?>
</div>
</div>

<div class="col-sm-3 news_container">
<div class="news_customer_group">
<div class='title_news_section'>OTHER NEWS</div>
<?php 
      $args=array(
      'cat' => 13,
      'offset' => 1,
      'posts_per_page'=> 6
      );
      $blogPosts = new WP_Query( $args );
        if( $blogPosts->have_posts() ) {
        //loop through related posts based on the tag
          echo '<ul class="other_news_ul">';

          while ( $blogPosts->have_posts() ) {

            $blogPosts->the_post();
              echo '<a href="'. get_the_permalink() .'">';
              echo '<li><strong>';
              the_title();
              echo '</strong></li>';
              echo '</a>';
          }
          echo '</ul>';
        }
        wp_reset_postdata();

?>
</div>
</div>

<div class="col-sm-3 news_container">
<div class="news_customer_group">
<div class='title_news_section'>OUR CUSTOMERS</div>
<img src="<?php bloginfo('stylesheet_directory')?>/images/customers/customers.jpg" alt="Whale shark watching"/>
<p>We had such a great time. The tour was well planned and arranged. The staff were very accommodating especially our driver guide. We would like to thank Whale Shark Oslob Tours for such a once in a lifetime great experience.</p>
</div>
</div>

<div class="col-sm-3 news_container">
<div class="news_customer_group">
<div class='title_news_section'>WRITE US YOUR THOUGHTS</div>
<div id="TA_rated444" class="TA_rated">
<ul id="L8hKDmBiNwf" class="TA_links txGs4uU">
<li id="HVq2jqRtejRe" class="fajTs1">
<a target="_blank" href="https://www.tripadvisor.com.ph/"><img src="https://www.tripadvisor.com.ph/img/cdsi/img2/badges/ollie-11424-2.gif" alt="TripAdvisor"/></a>
</li>
</ul>
</div>
<script async src="https://www.jscache.com/wejs?wtype=rated&amp;uniq=444&amp;locationId=14788548&amp;lang=en_PH&amp;display_version=2"></script>
</div>
</div>

</div> <!-- row -->


<div class="clearfix logo_section">
<div class="col-sm-3 logo_others">
<a href="http://https://www.paypal.com/ph/home/">
<img src="<?php bloginfo('stylesheet_directory')?>/images/logo/paypal.jpg" alt="Pay with Paypal"/>
</a>
  </div>
  <div class="col-sm-3 logo_others">
  <a href="http://www.tourism.gov.ph/">
<img src="<?php bloginfo('stylesheet_directory')?>/images/logo/itsmorefun.jpg" alt="Philippines Tourism"/>
 </a>
  </div>
  <div class="col-sm-3 logo_others">
  <a href="http://cebuboholtraveltours.com/">
<img src="<?php bloginfo('stylesheet_directory')?>/images/logo/logocebubohol.jpg" alt="Cebu Bohol Travel and Tours"/>
</a>
  </div>
  <div class="col-sm-3 logo_others">
  <a href="https://canyoneeringcebutours.com/">
<img src="<?php bloginfo('stylesheet_directory')?>/images/logo/logo-mail.jpg" alt="Kawasan Canyoneering"/>
</a>
  </div>
</div>

<?php if ( is_active_sidebar( 'bulk-footer-area' ) ) { ?>
	<div id="content-footer-section" class="row clearfix">
		<div class="container">
			<?php dynamic_sidebar( 'bulk-footer-area' ) ?>
		</div>
	</div>
<?php } ?>
</div>
<footer id="colophon" class="footer-credits container-fluid row">
	<div class="container">
	<?php do_action( 'bulk_generate_footer' ); ?>

	</div>
<div class="clearfix terms_section">
<a href="<?php bloginfo('stylesheet_directory')?>/downloads/wsot_terms_and_condition.pdf" target="_blank" class="btn btn-info" role="button">View Our Terms and Condition</a>
</div>  
</footer>
<!-- end main container -->
</div>
<?php wp_footer(); ?>
</body>
<script type="text/javascript">

jQuery(document).ready(function() {

    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#q_no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }

    for(i=1;i<=10;i++) {
      jQuery('#q_no_of_days').append('<option val="'+ i + '">' + i + '</option>');
    }

    // initialize the date picker
      var date_input=$('input[name="q_pick_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);


    // initialize the date picker
      var date_input=$('input[name="pick_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);

    submitQuick();

});


</script>
</html>
