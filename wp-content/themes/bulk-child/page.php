<?php get_header();

global $post;
// echo "<h1>" . $post->ID . "</h1>";
?>

<div class="top-header text-center">
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="single-image">
			<?php the_post_thumbnail( 'full' ); ?>
		</div>
	<?php endif; ?>
	<header class="header-title container">
		<div class="page-header">

		<?php // the_title(); ?>
		<?php if(is_front_page()): ?>
         <p style="font-size:.8em;color:#ffd802;line-height: 80%;">Whale Shark Oslob Tours<br/>
         <span style="font-size:.5em;color:#ffffff">Affordable and Easy Booking for Cebu South Tours and more ...</span><br/>
         <span style="font-size:.4em;color:#ffffff">Email : info@whalesharkoslob.com</span><br/>
         <span style="font-size:.4em;color:#ffffff">+63 917 704 3508 / +63 916 696 5614</span>
         </p>
		<?php elseif($post->ID==82): ?>
			<p style="font-size:.8em;color:#ffd802">We are Whale Shark Oslob Tours ...<br/>
			<span style="font-size:.5em;color:#ffffff">We provide you the best Cebu South Tours and more... </span>
            </p>
		<?php elseif($post->ID==142): ?>
			Experience our Day Tours ...
		<?php elseif($post->ID==145): ?>
			Enjoy overnight tours ...
		<?php elseif($post->ID==168): ?>
         <span style="font-size:.5em;color:#ffffff">Email : info@whalesharkoslob.com</span><br/>
         <span style="font-size:.5em;color:#ffffff">+63 917 704 3508 / +63 916 696 5614</span>
		<?php else: ?>
			SOMETHING ELSE ...
		<?php endif; ?>


		</div>
		<?php do_action( 'bulk_after_page_title' ); ?>
	</header>
</div>

<?php // get_template_part( 'template-parts/template-part', 'content' ); ?>


<div id="bulk-content" class="container main-container" role="main">
<?php
// echo "<h1>" . $post->ID . "</h1>";
if (is_front_page()) {
	get_template_part( 'parts/template', 'oslobtours' );
}elseif($post->ID==142) {
	get_template_part( 'parts/template', 'daytours' );
}elseif($post->ID==145) {
	get_template_part( 'parts/template', 'multidaytours' );
}
?>



<!-- start content container -->
<?php get_footer(); ?>
