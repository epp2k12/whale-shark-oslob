<div id="bulk-content" class="container main-container" role="main">
<!-- start content container -->
<div class="row" style="border:2px solid red;margin-top: 20px">
<!-- <h1> I AM INSIDE CONTENT SINGLE </h1> -->
	<article class="col-md-<?php bulk_main_content_width_columns(); ?>">
	<?php
	global $post;
	$this_slug = $post->ID;
	echo '<h1> The POST ID: '. $this_slug . '</h1>';
	?>
    <?php if( 'tour1' == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'oslobtumalogkawasan' ); ?>
    <?php else: ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div <?php post_class(); ?>>
					<div class="single-content">
						<div class="single-entry-summary">
							<?php the_content(); ?>
						</div><!-- .single-entry-summary -->
						<?php wp_link_pages(); ?>
					</div>
					<div class="single-footer row">
						<div class="col-md-4">
							<?php get_template_part( 'template-parts/template-part', 'postauthor' ); ?>
						</div>
						<div class="col-md-8">
							<?php comments_template(); ?>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>


	<?php endif; ?>
	</article>
	<?php get_sidebar( 'right' ); ?>
</div>
<!-- end content container -->
