<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-76084323-7"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-76084323-7');
	</script>
	<meta name="google-site-verification" content="bQ4TAPx9Hdi43PvKlOENGLafibjvYKwrbKVemi15tOw" />
	<meta name="msvalidate.01" content="94E032D67074B89854BC8FE811FAB4F7" />
	<meta name="keywords" content="whale shark oslob, whale shark, whale shark cebu, whale shark tours, whale shark tour, swim with whale sharks oslob, oslob whale shark, cebu whale shark, whale shark oslob tour, whale shark encounter, canyoneering cebu, canyoneering cebu tours, sumilon island, kawasan falls, kawasan canyoneering, mactan island hopping, malapascua island, kalanggaman island, cebu city tour, cebu city day tour, cebu tours" >

		<meta http-equiv="content-type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
		<?php wp_head(); ?>
	</head>
	<body id="blog" <?php body_class(); ?>>

	<!--  jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<link rel="profile" href="http://gmpg.org/xfn/11">
<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

		<?php get_template_part( 'template-parts/template-part', 'topnav' ); ?>

			<div class="page-area">
