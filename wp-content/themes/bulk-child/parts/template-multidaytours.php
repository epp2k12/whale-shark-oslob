<style type="text/css">


</style>

<div class="front_group">
<h3 class="pages_title_bars">ENJOY AFFORDABLE <strong style="color:#ff5000">OVERNIGHT(S)</strong> TOUR PACKAGES</h3>
<?php
$args=array(
	'cat' => 46,
	'order' => ASC
);
?>
	<?php
	$packagePosts = new WP_Query( $args );
	if( $packagePosts->have_posts() ) {
	//loop through related posts based on the tag
		while ( $packagePosts->have_posts() ) :
		$packagePosts->the_post();
	?>

	<div class="tour_package col-sm-3 col-lg-2 col-xs-6">
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail(); ?>

		<p class="dtitle">
		<?php the_title(); ?>
		</p>

	</a>
	<div class="book_now">
	<a href="<?php the_permalink(); ?>" class="btn btn-primary" role="button">BOOK NOW</a>
	</div>

	</div>

	<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php } ?>
</div>

<script type="text/javascript">

</script>