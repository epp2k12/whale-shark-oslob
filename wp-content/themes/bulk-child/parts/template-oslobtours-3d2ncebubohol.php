
<style type="text/css">


</style>
<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );

// echo '<h1>the discount' . $discount . '</h1>';
?>
<img src="<?php bloginfo('stylesheet_directory')?>/images/backgrounds/bkposts01.jpg" alt="Oslob Whale Shark" class="post_image active" />
<h4 class="title_bars">3 Days &amp; 2 Nights Cebu Bohol Tours</h4>

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>RATE/HEAD IS LESS ";
    echo $discount;
	echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="col-sm-6 tour_details">
<div class="image_swap">
<?php include(__DIR__  . '/template-swapimages.php'); ?>
</div>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 6 -->

<div class="col-sm-6 tour_details">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
					  <table class="table table-condensed">
					  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
					  <tr><td>1</td><td>16,150 / head</td></tr>
					  <tr><td>2</td><td>11,650 / head</td></tr>
					  <tr><td>3</td><td>9,950 / head</td></tr>
					  <tr><td>4</td><td>9,250 / head</td></tr>
					  <tr><td>5</td><td>8,650 / head</td></tr>
					  <tr><td>6</td><td>8,150 / head</td></tr>
					  <tr><td>7</td><td>7,650 / head</td></tr>
					  <tr><td>8</td><td>7,150 / head</td></tr>
					  <tr><td>9</td><td>6,650 / head</td></tr>
					  <tr><td>10</td><td>6,250 / head</td></tr>
					  <tr><td>11</td><td>5,850 / head</td></tr>
					  <tr><td>12</td><td>5,450 / head</td></tr>
					<tr><td>13</td><td>5150 / head</td></tr>
					  <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
					  <tr><td colspan="2">
							<strong>For Snorkeling with whale sharks:</strong>
							<ul>
							<li>Add on of Php 500/head for Foreign Guest</li>
				              <li>Whale watching viewing time is from 6AM to 11AM only</li>
				              <li>Whale watching time limit is within 30 minutes only</li>
							</ul>
					</td></tr>
					</table>

                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">
					<ul>
					<li>Accommodation</li>
					<ul><li>2 Nights stay at Robe's Pension in Cebu City</li></ul>
					<li>Whale Watching/swimming/snorkeling</li>
					<ul>
					<li>Entrance fee</li>
					<li>boat service</li>
					<li>life vest</li>
					<li>snorkeling gear</li>
					</ul>
					<li>Sumilon Island sandbar</li>
					<ul>
					<li>Entrance fee</li>
					<li>Roundtrip pumpboat service</li>
					</ul>
					<li>Cebu/Mactan City tour</li>
					<ul><li>Venue entrance fee</li></ul>
					<li>Bohol Countryside Tour</li>
					<ul>
					<li>Venue Entrance Fees</li>
					<li>Roundtrip Oceanjet Ferry Ticket</li>
					<li>Lunch at Loboc river cruise</li>
					</ul>
					<li>Private Fully air-condition vehicle service for the whole tour</li>
					</ul>
				  </td></tr>
				  </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
				  <table class="table table-condensed">
				  <tr><td colspan="2">
				  <ul>
				  <li>Meals (Breakfast, Lunch, Dinner)</li>
				  <li>Airfare</li>
				  <li>Underwater Cam</li>
				  <li>ATV and Zip ride fee at Bohol Tour</li>
          		  <li>Others not stated in the inclusion above</li>
				  </ul>
				  </td></tr>
				  </table>
                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
					<table class="table table-condensed">
					<tr><td colspan="2">Day 1 - Whale Watching, Sumilon, Tumalog Falls</td></tr>
					<tr><td style="width:35%">04:00 AM</td><td style="width:65%">Pick up at Hotel/Airport</td></tr>
					<tr><td>07:00am</td><td>Breakfast at Oslob</td></tr>
					<tr><td>07:30am</td><td>Whale shark watching/swimming</td></tr>
					<tr><td>08:00am</td><td>Depart to Sumilon island sandbar</td></tr>
					<tr><td>08:30am</td><td>Sumilon sandbar</td></tr>
					<tr><td>11:30am</td><td>Depart back to Mainland</td></tr>
					<tr><td>12:00nn</td><td>Lunch</td></tr>
					<tr><td>01:30pm</td><td>Depart to Tumalog Falls</td></tr>
					<tr><td>02:30pm</td><td>Depart to City</td></tr>
					<tr><td>05:30pm</td><td>Arrival and check-in at Robe's</td></tr>
					<tr><td colspan="2">Day 2 - Bohol Countryside Tour</td></tr>
					<tr><td>07:00am</td><td>Pickup at Robe's</td></tr>
					<tr><td>08:00am</td><td>Depart to Tagbilaran via Oceanjet</td></tr>
					<tr><td>10:00am</td><td>Arrival at Bohol then Pickup and start of the tour</td></tr>
					<tr><td colspan="2">
					<ul>
					<li>Blood Compact Site</li>
					<li>Baclayon Church</li>
					<li>St. Peter Ruins Church</li>
					<li>River Cruise Floating Restaurant with Buffet Lunch</li>
					<li>Bilar Man-made Forest</li>
					<li>Tarsier Visit</li>
					<li>Chocolate Hills</li>
					<li>Butterfly Garden</li>
					</ul>
					</td></tr>
					<tr><td>05:40pm</td><td>Depart to Cebu via Oceanjet</td></tr>
					<tr><td>07:40pm</td><td>Arrival and pickup at Pier1</td></tr>
					<tr><td>08:00pm</td><td>Arrival at Robe's</td></tr>
					<tr><td colspan="2">Day 3 - Cebu/Mactan City Tour</td></tr>
					<tr><td>09:00am</td><td>Check-out and Pickup at Robe's</td></tr>
					<tr><td>09:30am</td><td>Fort San Pedro, Magellan's Cross, Santo Nino Church</td></tr>
					<tr><td>10:30am</td><td>Cebu Cathedral Church, Cebu Heritage Monument, Sugbo Museo</td></tr>
					<tr><td>12:00nn</td><td>Lunch</td></tr>
					<tr><td>01:00pm</td><td>Pasalubong Shop, Taoist temple, Mactan Shrine</td></tr>
					<tr><td>05:00pm</td><td>Arrival at Mactan Airport</td></tr>
					</table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->
</div> <!-- col 6 -->
<?php get_template_part( 'parts/template', 'footer-tours' ); ?>

<script type="text/javascript">

  var package_tour_name = "3 DAYS & 2 NIGHTS CEBU BOHOL TOUR PACKAGE";
  var pkprice_per_head = [16150,11650,9950,9250,8650,8150,7650,7150,6650,6250,5850,5450,5150];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

  	setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });


</script>


