
<style type="text/css">

</style>
<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );

// echo '<h1>the discount' . $discount . '</h1>';
?>
<img src="<?php bloginfo('stylesheet_directory')?>/images/backgrounds/bkposts01.jpg" alt="Oslob Whale Shark" class="active" />


<div class="col-sm-6 tour_details">
<h4 class="title_bars">OSLOB OVERNIGHT (2D/1N) FULL ADVENTURE TOUR PACKAGE</h4>

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>RATE/HEAD IS LESS ";
    echo $discount;
	echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<?php include(__DIR__  . '/template-swapimages.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 6 -->

<div class="col-sm-6 tour_details">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
					  <table class="table table-condensed">
					  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
					  <tr><td>1</td><td>15,500 / head</td></tr>
					  <tr><td>2</td><td>10,800 / head</td></tr>
					  <tr><td>3</td><td>9,700 / head</td></tr>
					  <tr><td>4</td><td>8,400 / head</td></tr>
					  <tr><td>5</td><td>7,100 / head</td></tr>
					  <tr><td>6</td><td>6,400 / head</td></tr>
					  <tr><td>7</td><td>6,000 / head</td></tr>
					  <tr><td>8</td><td>5,400 / head</td></tr>
					  <tr><td>9</td><td>5,000 / head</td></tr>
					  <tr><td>10</td><td>4,800 / head</td></tr>
					  <tr><td>11</td><td>4,600 / head</td></tr>
					  <tr><td>12</td><td>4,400 / head</td></tr>
					  <tr><td>13 and Above</td><td>Contact us for the price quotation</td></tr>
	          		  <tr>
	          		  <tr><td colspan="3">
							<strong>Important notes:</strong>
							<ul>
							<li>*** For foreign guest, it has an add-on fee of P500/head in whale shark and not included in this package.</li>
							<li>*** Children below 12yo are not allowed at Canyoneering. Children with parents or guardians will go directly to Kawasan Falls</li>
							<li>*** Side Trip is subject to surcharge</li>
							</ul>
					  </td></tr>
					  </table>

                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">
				  <ul>

					<li>Fully Air-conditioned  Transportation for 2 days</li>
					<li>Room accommodation at Lagnasonâ€™s Place Resort 1N</li>
					<li>Whale Watching and Snorkeling Fees</li>
					<li>Life Jacket and Snorkeling gear in whale watching</li>
					<li>Sumilon Island Sandbar Fees</li>
					<li>Boat Ride for Sumilon Island</li>
					<li>Tumalog Falls Venue Fees</li>
					<li>Motorbike ride fees for Tumalog Falls</li>
					<li>Aguinid Falls Venue Fees</li>
					<li>Kawasan Falls Fees</li>
					<li>Canyoneering fees</li>
					<li>Canyoneering local guide, life vest, safety helmet, aqua shoes</li>
					<li>Free use / Night Swimming at the pool of the resort</li>

				  </ul>
				  </td></tr>
				  </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">
				  <ul>

					<li>Underwater Camera</li>
					<li>Meals (Breakfast, Lunch &amp; Dinner)</li>
					<li>Airfare</li>
					<li>Snorkeling gear in sumilon</li>
					<li>Bluewater resort in sumilon</li>

				  </ul>
				  </td></tr>
				  </table>

                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
				  <table class="table table-condensed">
		          <tr><td colspan="2" style="background-color:#faffaf">DAY 1</td></tr>
		          <tr><td style="width:35%">05:00 AM</td><td style="width:65%">Pickup within Metro Cebu or Airport</td></tr>
		          <tr><td>06:00 AM</td><td>Breakfast at Carcar</td></tr>
		          <tr><td>07:30 AM</td><td>Arrival at Osme&ntilde;a Peak</td></tr>
		          <tr><td>08:30 AM</td><td>Depart to Oslob</td></tr>
		          <tr><td>09:30 AM</td><td>Depart for Tumalog Falls</td></tr>
		          <tr><td>11:30 PM</td><td>Depart back to Resort</td></tr>
		          <tr><td>12:00 NN</td><td>Check-in and Lunch</td></tr>
		          <tr><td>01:00 PM</td><td>Depart to Sumilon Island</td></tr>
		          <tr><td>04:00 PM</td><td>Depart to Oslob</td></tr>
		          <tr><td>05:00 PM</td><td>Arrival at Resort</td></tr>
		          <tr><td>06:00 PM</td><td>Dinner at Resort and Relax</td></tr>
		          <tr><td colspan="2"  style="background-color:#faffaf">DAY 2</td></tr>
		          <tr><td style="width:35%">05:00 AM</td><td style="width:65%">Breakfast at Resort</td></tr>
		          <tr><td>06:00 AM</td><td>Checkout and Depart from Resort </td></tr>
		          <tr><td>06:15 AM</td><td>Whale Watching/Swimming/Snorkeling</td></tr>
		          <tr><td>08:00 AM</td><td>Depart to Aguinid Falls</td></tr>
		          <tr><td>09:00 AM</td><td>Arrival at Aguinid Falls</td></tr>
		          <tr><td>10:30 AM</td><td>Depart to Badian</td></tr>
		          <tr><td>11:30 AM</td><td>Arrival at Badian and Lunch</td></tr>
		          <tr><td>12:30 PM</td><td>Start Canyoneering</td></tr>
		          <tr><td>03:30 PM</td><td>Cool off at Kawasan Falls</td></tr>
		          <tr><td>04:30 PM</td><td>Depart to Cebu City</td></tr>
		          <tr><td>07:00 PM</td><td>Arrival in Cebu City</td></tr>
				  </table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->
</div> <!-- col 6 -->
<?php get_template_part( 'parts/template', 'footer-tours' ); ?>
<script type="text/javascript">
  var package_tour_name = "OSLOB OVERNIGHT (2D/1N) FULL ADVENTURE";
  var pkprice_per_head = [15500,10800,9700,8400,7100,6400,6000,5400,5000,4800,4600,4400];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

  	setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>


