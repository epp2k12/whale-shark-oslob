
<style type="text/css">


</style>
<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );

// echo '<h1>the discount' . $discount . '</h1>';
?>
<img src="<?php bloginfo('stylesheet_directory')?>/images/backgrounds/bkposts01.jpg" alt="Oslob Whale Shark" class="post_image active" />


<div class="col-sm-6 tour_details">
<h4 class="title_bars">Malapascua Island Day Tour</h4>

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>RATE/HEAD IS LESS ";
    echo $discount;
	echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>
<iframe width="560" height="315" src="https://www.youtube.com/embed/OZtpxNJXsq8?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 6 -->

<div class="col-sm-6 tour_details">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
					  <table class="table table-condensed">
					  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
					  <tr><td>1</td><td>13,500/head</td></tr>
					  <tr><td>2</td><td>6,950/head</td></tr>
					  <tr><td>3</td><td>5,500/head</td></tr>
					  <tr><td>4</td><td>5,100/head</td></tr>
					  <tr><td>5</td><td>4,600/head</td></tr>
					  <tr><td>6</td><td>3,900/head</td></tr>
					  <tr><td>7</td><td>3,600/head</td></tr>
					  <tr><td>8</td><td>3,300/head</td></tr>
					  <tr><td>9</td><td>3,000/head</td></tr>
					  <tr><td>10</td><td>2,900/head</td></tr>
					  <tr><td>11</td><td>2,800/head</td></tr>
					  <tr><td>12</td><td>2,700/head</td></tr>
					  <tr><td>13</td><td>2,600/head</td></tr>
					  <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
					  </table>

                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">
		          Private Fully Air-conditioned car or van service â€“ Roundtrip transfer from City-Maya Port <br/>
		          Roundtrip Pumpboat Service to/from Maya port to Malapascua island <br/>
		          Life Jacket <br/>
		          Meals: Breakfast and Lunch <br/>
		          Tour Guide Fee <br/>
		          Private charter boat for island tours <br/>
				  </td></tr>
				  </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
				  <table class="table table-condensed">
				  <tr><td colspan="2">

					  Air Fare<br/>
			          Accommodation<br/>
			          Underwater Camera<br/>
			          Snorkeling gear<br/>
			          Meal (Dinner)<br/>

				  </td></tr>
				  </table>
                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
				  <table class="table table-condensed">
          <tr><td style="width:35%">04:00am </td><td>   Pickup at Hotel/Airport</td></tr>
          <tr><td>07:00am </td><td>  Arrival at Maya Port </td></tr>
          <tr><td>07:15am </td><td>  Depart to Malapascua island </td></tr>
          <tr><td>08:00am </td><td>  Arrival at Malapascua resort</td></tr>
          <tr><td>08:30am </td><td>  Breakfast</td></tr>
          <tr><td>09:00am </td><td>  Malapascua Tour (Swimming/Snorkeling)</td></tr>
          <tr><td>12:00nn </td><td>  Lunch</td></tr>
          <tr><td>03:00pm </td><td>  Depart to Maya Port</td></tr>
          <tr><td>04:00pm </td><td>  Arrival at Maya Port</td></tr>
          <tr><td>04:15pm </td><td>  Depart to Cebu City</td></tr>
          <tr><td>07:30pm </td><td>  Arrival Cebu City</td></tr>
				  </table>
                </div>
            </div>
          </div>


</div> <!-- accordion -->
</div> <!-- col 6 -->
<?php get_template_part( 'parts/template', 'footer-tours' ); ?>
<script type="text/javascript">

  var package_tour_name = "MALAPASCUA ISLAND DAY TOUR";
  var pkprice_per_head = [13500,6950,5500,5100,4600,3900,3600,3300,3000,2900,2800,2700,2600];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

  	setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>


