<style type="text/css">

</style>

<div class="front_group">
<h2 class="pages_title_bars">WHALE SHARK CEBU TOUR PACKAGES - POPULAR </h2>
<?php
$args=array(
	'cat' => 3,
	'order' => ASC
);
?>
	<?php
	$packagePosts = new WP_Query( $args );
	if( $packagePosts->have_posts() ) {
	//loop through related posts based on the tag
		while ( $packagePosts->have_posts() ) :
		$packagePosts->the_post();
	?>

	<div class="tour_package col-sm-3 col-lg-2 col-xs-6">
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail(); ?>

		<p class="dtitle">
		<?php the_title(); ?>
		</p>

	</a>
	<div class="book_now">
	<a href="<?php the_permalink(); ?>" class="btn btn-primary" role="button">BOOK NOW</a>
	</div>

	</div>

	<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php } ?>
</div>

<div class="clearfix"></div>

<div class="front_group">
<h2 class="pages_title_bars"><strong style="color:#ff5000">MULTI-DAYS</strong> WHALE SHARK CEBU TOUR PACKAGES</h2>
<?php
$args=array(
	'cat' => 4,
	'order' => ASC
);
?>
	<?php
	$packagePosts = new WP_Query( $args );
	if( $packagePosts->have_posts() ) {
	//loop through related posts based on the tag
		while ( $packagePosts->have_posts() ) :
		$packagePosts->the_post();
	?>
	<div class="tour_package col-sm-3 col-lg-2 col-xs-6">
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail(); ?>

		<p class="dtitle">
		<?php the_title(); ?>
		</p>

	</a>
	<div class="book_now">
	<a href="<?php the_permalink(); ?>" class="btn btn-primary" role="button">BOOK NOW</a>
	</div>
	</div>
	<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php } ?>
</div>

<div class="clearfix"></div>

<div class="front_group">
<h3 class="pages_title_bars"><strong style="color:#fdf12c">CEBU TOUR PACKAGES</strong></h3>
<?php
$args=array(
	'cat' => 5,
	'order' => ASC
);
?>
	<?php
	$packagePosts = new WP_Query( $args );
	if( $packagePosts->have_posts() ) {
	//loop through related posts based on the tag
		while ( $packagePosts->have_posts() ) :
		$packagePosts->the_post();
	?>
	<div class="tour_package col-sm-3 col-lg-2 col-xs-6">
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail(); ?>

		<p class="dtitle">
		<?php the_title(); ?>
		</p>

	</a>
	<div class="book_now">
	<a href="<?php the_permalink(); ?>" class="btn btn-primary" role="button">BOOK NOW</a>
	</div>
	</div>
	<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php } ?>
</div>


<script type="text/javascript">



</script>