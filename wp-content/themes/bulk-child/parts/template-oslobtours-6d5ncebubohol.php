
<style type="text/css">

</style>
<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );

// echo '<h1>the discount' . $discount . '</h1>';
?>
<img src="<?php bloginfo('stylesheet_directory')?>/images/backgrounds/bkposts01.jpg" alt="Oslob Whale Shark" class="active" />
<h4 class="title_bars">6 Days &amp; 5 Nights Ultimate Cebu &amp; Bohol Tour</h4>

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>RATE/HEAD IS LESS ";
    echo $discount;
	echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="col-sm-6 tour_details">
<div class="image_swap">
<?php include(__DIR__  . '/template-swapimages.php'); ?>
</div>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 6 -->

<div class="col-sm-6 tour_details">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
					  <table class="table table-condensed">
					  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
					  <tr><td>1</td><td>41,150 / head</td></tr>
					  <tr><td>2</td><td>24,550 / head</td></tr>
					  <tr><td>3</td><td>18,817 / head</td></tr>
					  <tr><td>4</td><td>16,100 / head</td></tr>
					  <tr><td>5</td><td>14,440 / head</td></tr>
					  <tr><td>6</td><td>13,500 / head</td></tr>
					  <tr><td>7</td><td>12,929 / head</td></tr>
					  <tr><td>8</td><td>11,925 / head</td></tr>
					  <tr><td>9</td><td>11,161 / head</td></tr>
					  <tr><td>10</td><td>10,720 / head</td></tr>
					  <tr><td>11</td><td>10,150 / head</td></tr>
					  <tr><td>12</td><td>9,692 / head</td></tr>
					  <tr><td>13</td><td>9,512 / head</td></tr>
            		  <tr><td>14</td><td>9,143 / head</td></tr>
					  <tr><td>15 and Above</td><td>Contact us for the price quotation</td></tr>
					  <tr><td colspan="2">
							<strong>For Snorkeling with whale sharks:</strong>
							<ul>
							<li>•  For foreign guest, it has an add-on fee of P500/head in whale shark and not included in this package.</li>
			                <li>•  Children below 12yo are not allowed at Canyoneering. Children with parents or guardians will go directly to Kawasan Falls</li>
			                <li>•  Side trip is subject to surcharge</li>
							</ul>
					</td></tr>
					</table>

                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">
					<ul>
					<li>Accommodation<ul>
					<li>2Nights stay at Lagnason's Place Resort in Oslob</li>
					<li>3Nights stay at Robe’s Pension in Cebu City</li>
					</ul>
					</li>
					<li>Osmena Peak</li>
					<li>Whale Watching/swimming/snorkeling
					<ul>
					<li>Entrance fee</li>
					<li>boat service</li>
					<li>life vest</li>
					<li>snorkeling gear</li>
					</ul>
					</li>
					<li>Tumalog Falls
					<ul>
					<li>Entrance fee</li>
					<li>Motorbike service</li>
					</ul>
					</li>
					<li>Simala Shrine</li>
					<li>Pescador island hopping
					<ul>
					<li>entrance fee</li>
					<li>lifevest, snorkeling gear</li>
					<li>pumpboat service</li>
					<li>boatman as guide</li>
					</ul>
					</li>
					<li>Bohol Day Tour
					<ul>
					<li>Pickup service from Hotel to Wharf</li>
					<li>Round Trip Fast Craft Ticket (Oceanjet)</li>
					<li>Buffet Lunch at Floating Restaurant</li>
					<li>Venue Entrance Fees</li>
					<li>Fully Air-conditioned Van for Bohol Transport</li>
					<li>Pick-up Service from Cebu Wharf to Cebu Hotel</li>
					</ul>
					</li>
					<li>Kawasan Canyoneering
					<ul>
					<li>entrance fee</li>
					<li>motorbike service to jump off point</li>
					<li>local canyoneering tour guide</li>
					<li>life vest</li>
					<li>rubber shoes</li>
					<li>safety helmet</li>
					</ul>
					</li>
					<li>Malapascua island tour
					<ul>
					<li>Entrance fee</li>
					<li>Pumpboat service</li>
					<li>Tour guide</li>
					<li>Lunch</li>
					</ul>
					</li>
					<li>City Tour
					<ul>
					<li>Venue Entrance Fees</li>
					</ul>
					</li>
					<li>Private Fully air-condition vehicle service for the whole tour
					<ul>
					<li>1-6 pax Toyota innova</li>
					<li>7-14pax Toyota Hiace</li>
					</ul>
					</li>
					</ul>
				  </td></tr>
				  </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
				  <table class="table table-condensed">
				  <tr><td colspan="2">
				  <ul>
		          <li>Meals</li>
		          <li>Airfare</li>
		          <li>underwater cam</li>
		          <li>ATV and Zip ride at Bohol</li>
		          <li>snorkeling gear in Malapascua</li>
		          <li>Others not stated in the inclusions above</li>
				  </ul>
				  </td></tr>
				  </table>
                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
				<table class="table table-condensed">
				<tr><td colspan="2"><strong>Day 1</strong></td></tr>
				<tr><td>05:00am</td><td>Pickup at mactan airport</td></tr>
				<tr><td>06:00am</td><td>Breakfast at City</td></tr>
				<tr><td>07:30am</td><td>Depart to Simala</td></tr>
				<tr><td>09:00am</td><td>Simala Shrine</td></tr>
				<tr><td>11:30am</td><td>Lunch</td></tr>
				<tr><td>01:00pm</td><td>Depart to Osmena peak</td></tr>
				<tr><td>02:30pm</td><td>Osmena Peak</td></tr>
				<tr><td>04:30pm</td><td>Depart to Oslob</td></tr>
				<tr><td>05:30pm</td><td>Check-in at Lagnason's Place Resort in Oslob</td></tr>
				<tr><td colspan="2"><strong>Day 2</strong></td></tr>
				<tr><td>05:30am</td><td>Breakfast at resort</td></tr>
				<tr><td>05:50am</td><td>Depart to Whale watching area</td></tr>
				<tr><td>06:10am</td><td>Whale shark watching/swimming</td></tr>
				<tr><td>08:00am</td><td>Tumalog Falls</td></tr>
				<tr><td>09:00am</td><td>Depart to Moalboal</td></tr>
				<tr><td>11:00am</td><td>Arrival at Moalboal then lunch</td></tr>
				<tr><td>11:30am</td><td>Start Pescador island hopping</td></tr>
				<tr><td colspan="2">
				<ul>
				<li>• turtle watching</li>
				<li>• sardine run</li>
				<li>• fish feeding</li>
				<li>• swimming/snorkeling</li>
				</ul>
				<strong>Note:</strong> turtle watching and sardine run are not guaranteed it will depend on the weather and timing
				</td></tr>
				<tr><td>04:00pm</td><td>   Back to Oslob</td></tr>
				<tr><td>05:30pm</td><td>  Arrival at resort</td></tr>
				<tr><td colspan="2"><strong>Day 3</strong></td></tr>
				<tr><td>05:30am</td><td>   Breakfast at resort</td></tr>
				<tr><td>06:00am</td><td>   Check-out and Depart to Badian</td></tr>
				<tr><td>08:00am</td><td>   Start Canyoneering</td></tr>
				<tr><td>12:00nn</td><td>   Lunch at Kawasan</td></tr>
				<tr><td>01:00pm</td><td>   Cool off at Kawasan Falls</td></tr>
				<tr><td>04:00pm</td><td>   Depart to City</td></tr>
				<tr><td>07:00pm</td><td>   Arrival and Check-in at Robe's Pension</td></tr>
				<tr><td colspan="2">Day 4</td></tr>
				<tr><td>04:30am</td><td>Pickup at Robe’s
				<tr><td>06:00am</td><td>Breakfast at fastfood
				<tr><td>08:00am</td><td>Arrival and depart from Maya port
				<tr><td>09:00am</td><td>Start Malapascua tour
				<tr><td colspan="2">
				<ul>
				<li>• Bounty beach</li>
				<li>• Langub beach</li>
				<li>• Coral garden</li>
				<li>• Japanese ship wreck</li>
				</ul>
				</td></tr>
				<tr><td>12:00nn</td><td>Lunch (free)</td></tr>
				<tr><td>02:00pm</td><td>Cool off</td></tr>
				<tr><td>03:00pm</td><td>Depart to Maya port</td></tr>
				<tr><td>04:00pm</td><td>Depart to City</td></tr>
				<tr><td>07:00pm</td><td>Arrival at Robe’s</td></tr>
				<tr><td colspan="2"><strong>Day 5</strong></td></tr>
				<tr><td>06:00am</td><td>   Pickup at Hotel</td></tr>
				<tr><td>06:30am</td><td>   Breakfast at fastfood</td></tr>
				<tr><td>08:00am</td><td>   Depart to Bohol via Oceanjet</td></tr>
				<tr><td>10:00am</td><td>   Arrival at Bohol then start tour</td></tr>
				<tr><td colspan="2">
				<ul>
				<li>• Blood Compact Site</li>
				<li>• Baclayon Church</li>
				<li>• St. Peter's Ruins Church</li>
				<li>• Bohol Biggiest Snake in Captivity</li>
				<li>• River Cruise Floating Resturant w/ Buffet Lunch</li>
				<li>• Bilar Man-made Forest</li>
				<li>• Tarsier Visit</li>
				<li>• Chocolate Hills</li>
				<li>• Butterfly Garden</li>
				</ul>
				</td></tr>
				<tr><td>04:00pm</td><td>   End of Tour Proceed to Sea Port</td></tr>
				<tr><td>05:30pm</td><td>   Depart to Cebu City via Oceanjet</td></tr>
				<tr><td>07:30pm</td><td>   Pickup from Cebu Sea port/wharf then drop off to hotel</td></tr>
				<tr><td colspan="2"><strong>Day 6</strong></td></tr>
				<tr><td>09:00am</td><td>   Check out and start city tour</td></tr>
				<tr><td>09:30am</td><td>   Fort San Pedro, Magellan’s Cross, Santo Nino Church, Cebu Cathedral, Cebu Heritage monument, Yap Sandiego House</td></tr>
				<tr><td>12:00nn</td><td>   Lunch</td></tr>
				<tr><td>01:00pm</td><td>   Pasalubong (souvenir) shop, Taoist temple, Mactan Shrine</td></tr>
				<tr><td>05:00pm</td><td>   Arrival at Mactan Airport</td></tr>
				<tr><td colspan="2"><strong>End of tour and service</strong></td></tr>
				</table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->
</div> <!-- col 6 -->
<?php get_template_part( 'parts/template', 'footer-tours' ); ?>
<script type="text/javascript">
  var package_tour_name = "6 DAYS 5 NIGHTS ULTIMATE CEBU BOHOL TOUR PACKAGE";
  var pkprice_per_head = [41150,24550,18817,16100,14440,13500,12929,11925,11161,10720,10150,9692,9512,9143];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

  	setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>


