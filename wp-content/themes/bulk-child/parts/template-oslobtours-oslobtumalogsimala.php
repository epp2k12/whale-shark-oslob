
<style type="text/css">

</style>
<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );

// echo '<h1>the discount' . $discount . '</h1>';
?>
<img src="<?php bloginfo('stylesheet_directory')?>/images/backgrounds/bkposts01.jpg" alt="Oslob Whale Shark" class="active" />


<div class="col-sm-6 tour_details">
<h4 class="title_bars">OSLOB WHALE * TUMALOG FALLS * SIMALA SHRINE</h4>

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>RATE/HEAD IS LESS ";
    echo $discount;
	echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<?php include(__DIR__  . '/template-swapimages03.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 6 -->

<div class="col-sm-6 tour_details">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
					  <table class="table table-condensed">
					  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
					  <tr><td>1</td><td>5,450head</td></tr>
					  <tr><td>2</td><td>3,350/head </td></tr>
					  <tr><td>3</td><td>2,800/head</td></tr>
					  <tr><td>4</td><td>2,400/head</td></tr>
					  <tr><td>5</td><td>2,200/head </td></tr>
					  <tr><td>6</td><td>1,950/head</td></tr>
					  <tr><td>7</td><td>1,850/head</td></tr>
					  <tr><td>8</td><td>1,800/head </td></tr>
					  <tr><td>9</td><td>1,750/head</td></tr>
					  <tr><td>10</td><td>1,700/head</td></tr>
					  <tr><td>11</td><td>1,650/head</td></tr>
					  <tr><td>12</td><td>1,600/head</td></tr>
					  <tr><td>13</td><td>1,550/head</td></tr>
					  <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
					  <tr>
					  <tr><td colspan="3">
							<strong>Important notes:</strong>
							<ul>
							<li>*** For foreign guest, it has an add-on fee of P500/head in whale shark and not included in this package.</li>
							<li>*** Side Trip is subject to surcharge</li>
							</ul>
					  </td></tr>
					  </table>

                  </div>
              </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">
				  Whale watching and Tumalog falls entrance<br>
				  Boat ride and life vest<br>
				  Whale shark watching<br>
				  Tumalog Falls<br>
				  Fully air-conditioned Car or Van Service<br>
				  </td></tr>
				  </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">
				  MEALS/Snacks<br>
				  Underwater Camera<br>
				  Air Fare<br/>
				  Accomodations<br/>
				  </td></tr>
				  </table>

                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
				  <table class="table table-condensed">
				  <tr><td colspan="2">Recommended pick up time from Cebu City or Lapu-lapu City is between 4:00AM to 5:30AM</td></tr>
					<tr><td style="width:35%">05:00 AM </td><td style="width:65%"> Pick up and departure time from Hotel </td></tr>
					<tr><td>08:00 AM </td><td> Arrival at Oslob then Breakfast </td></tr>
					<tr><td>08:30 AM </td><td> Whale Shark Watching </td></tr>
					<tr><td>09:30 AM </td><td> Tumalog Falls </td></tr>
					<tr><td>11:30 AM </td><td> Lunch </td></tr>
					<tr><td>12:30 PM </td><td> Depart from Oslob for Simala</td></tr>
					<tr><td>02:30 PM </td><td> Arrival Simala Shrine</td></tr>
					<tr><td>03:30 PM </td><td> Depart to Cebu City</td></tr>
					<tr><td>06:00 PM </td><td> Arrival Cebu City </td></tr>
				  <tr><td colspan="2">In case you want to have side trip (upon arrival in Cebu City), we charge P300.00/hr. </td></tr>
				  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-hand-right"></span> DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">1 Day private tour (15 hours duration)</td></tr>
				  <tr><td colspan="2">Tour Facilitator</td></tr>
				  <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
				   <tr><td colspan="2">
					 <ul>
					 <li>1 to 3 persons &#45; sedan type vehicle</li>
					 <li>4 to 6 persons &#45; minivan type vehicle</li>
					 <li>7 to 13 persons &#45; van type vehicle</li>
					 <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
					 </ul>
				   </td></tr>
				  </table>

                </div>
            </div>
          </div>

</div> <!-- accordion -->
</div> <!-- col 6 -->

<div class="package-caption">
<h1>Oslob whale shark watching, Tumalog Falls and Simala</h1>
<p>
An exciting day tour package for your group (friends and family). We kick off as we pick your group early 5:00AM with a private fully air-conditioned vehicle (van or car depends on number of persons). Pick-up is free within Cebu City area or Mactan Area (Airport or Hotel). From Cebu we travel about 3 hours to reach Oslob, Tan-awan town. All entrance fees, motorcycle ride and boat rides are included in the package. For the oslob whale shark watching you will be provided with life vest. You will then swim with the whale sharks ( there are rules not to touch the whales ) for about 30 minutes. You can take pictures with the whale sharks but you must provide your own underwater camera. If you don't have one you can easily rent a camera on sight for about P400-500. No worries, if you don't know how to swim the life vest will keep you afloat and the boat men in charge will help you and keep watch of you and your group. You will surely cherish this great experience of Oslob whale shark watching and swimming with the giants of the seas.
</p>
<p>
Then we head off to Tumalog falls around 10:00 and there you will see one of the most beautiful water falls in Oslob. It is like a great wall with water falling like rain drops. You will enjoy taking pictures or even taking a quick dip in its cool waters.
</p>
<p>
Last off will be a tour to the most holy shrine, the Simala Shrine of Sibonga. It is a very huge castle like shrine for the catholic devotees. Many believers come here in Simala shrine to pray and make their petitions.
</p>
<p>
This day tour package is very wholesome and very affordable. So hurry book this tour now. Oslob <a href="https://whalesharkoslob.com/oslob-tours-whale-shark-tumalogsimala/">whale shark watching</a>, Tumalog falls and Simala Shrine tour package.
</p>
</div> <!-- package caption -->

<?php get_template_part( 'parts/template', 'footer-tours' ); ?>
<script type="text/javascript">
  var package_tour_name = "OSLOB WHALE / TUMALOG FALLS / SIMALA";
  var pkprice_per_head = [5450,3350,2800,2400,2200,1950,1850,1800,1750,1700,1650,1600,1550];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

    setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>


