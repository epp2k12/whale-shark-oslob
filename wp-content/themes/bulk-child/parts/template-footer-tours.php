<div class="clearfix"></div>
<div class="front_group" style="margin-bottom:5px">
<h3 class="pages_title_bars"><strong style="color:#fdf12c;text-align:center">OTHER FAVORITE TOUR PACKAGES</strong></h3>
<?php
$args=array(
  'posts_per_page' => 20,
  'category__in' => array(16), //change and add the category ids here
  'orderby' => 'date',
  'order' => 'ASC'
);
?>
  <?php
  $packagePosts = new WP_Query( $args );
  if( $packagePosts->have_posts() ) {
  //loop through related posts based on the tag
    while ( $packagePosts->have_posts() ) :
    $packagePosts->the_post();
  ?>
  <div class="tour_package col-sm-6 col-lg-2 col-xs-6">
  <a href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail(); ?>
  </a>

    <p class="dtitle">
    <?php the_title(); ?>
    </p>

  </div>
  <?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php } ?>
</div>