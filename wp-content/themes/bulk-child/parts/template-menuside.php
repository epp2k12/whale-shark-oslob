<style type="text/css">
#form_compute table, #form_compute tr, #form_compute td {
  /* border: 1px solid blue; */
}

#form_compute {
  padding:2px;
  background-color:#6cc9ff; 
}

#form_compute input {
	margin:0px;
  color:gray;
  font-size:1em;
}



#table_compute, #table_compute td, #table_compute tr, textarea {
  padding:1px 5px;
  margin:0px;
}

#reserve_14:hover{
  color:#6cc9ff;
}

  </style>

  <form id="form_compute">
  <input type="hidden" name="submitted" value="true">
  <input type="hidden" name="tour_name" id="tour_name" value="">

  <table class="table table-condensed table_compute" id="table_compute" >

  <tr><td colspan="4" style="background-color:#000000; color:white;">PLEASE FILL-UP REQUIRED DETAILS</td></tr>
        <tr><td colspan="2">
        Full Name <span class="required">*</span>
        </td>
        <td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="first_name" id="first_name" placeholder="Enter Full Name" required>
        </td>
        </tr>
        <tr><td colspan="2">
        Contact <span class="required">*</span>
        </td><td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="contact" id="contact" placeholder="Enter Contact Number">
        </td></tr>
        <tr><td colspan="2">
        Email <span class="required">*</span>
        </td><td colspan="2">
        <input type="email" class="form-control compute_form_required_control" name="email" id="email" placeholder="Enter Best Email" required>
        </td></tr>
        <tr><td colspan="2">
        PickUp Location <span class="required">*</span>
        </td><td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="pick_location" id="pick_location" placeholder="Airport/Hotel">
        </td></tr>
        <tr><td colspan="2">
        PickUp Date <span class="required">*</span>
        </td><td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="pick_date" id="pick_date" placeholder="mm/dd/yyyy" required>
        </td></tr>
        <tr><td colspan="4">
        ADDITIONAL INFORMATION<br/>
        <textarea class="form-control noresize" name="other_info" id="other_info"  rows="3" placeholder="Additional Info"></textarea>
        </td></tr>

  <tr><td colspan="2"><span class="impt_text"><strong>NO. OF PERSONS *</strong></td>
  <td colspan="2">
    <select name="no_of_persons" id="no_of_persons" style="width:100%">
    </select>
  </td>
  </tr>
        <tr><td colspan="2">
        RATE/HEAD:
        </td><td colspan="2">
        <input type="text" class="form-control" name="price_head_adult" id="price_head_adult" readonly>
        </td></tr>

        <tr><td colspan="2">
        TOTAL RATE:
        </td><td colspan="2">
        <input type="text" class="form-control" name="computed_total_price" id="computed_total_price" readonly>
        </td></tr>

        <tr><td colspan="4" style="text-align:center">
        <input  type="submit" value="RESERVE NOW" id="reserve_14" style="background-color:#ffea00;height:60px;font-size:1.3em;border:0px"><br/>
        </td></tr>
 
  </table>

  </form>



