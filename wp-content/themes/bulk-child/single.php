<?php get_header(); ?>

<?php
	global $post;
	$this_ID = $post->ID;
	//echo '<h1> The POST ID: '. $this_ID . '</h1>';
?>
<!-- <h1> YOU ARE INSIDE A SINGLE PAGE!</h1> -->

<?php
//Remember to put the post ID of posts for tours
$postTours = array(35,38,88,91,94,97,100,130,133,136,176,179,182,185,188,191);
if (!in_array($this_ID, $postTours)) {
?>
<div class="top-header text-center">
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="single-image">
			<?php the_post_thumbnail( 'full' ); ?>
		</div>
	<?php endif; ?>
	<header class="header-title container">
		<h1 class="page-header">
			<?php the_title(); ?>
		</h1>
		<div class="post-meta">
			<?php bulk_time_link(); ?>
			<?php bulk_posted_on(); ?>
			<?php bulk_entry_footer(); ?>
		</div>
		<?php do_action( 'bulk_after_post_meta' ); ?>
	</header>
</div>
<?php
}
?>
<?php // get_template_part( 'template-parts/template-part', 'content' ); ?>
<?php // get_template_part( 'content', 'single' ); ?>

<div id="bulk-content" class="container main-container" role="main">
<!-- start content container -->
<div class="row" style="margin-top:20px">
<!-- <h1> I AM INSIDE CONTENT SINGLE </h1> -->
<?php if (!in_array($this_ID, $postTours)): ?>
	<article class="col-md-<?php bulk_main_content_width_columns(); ?>">
<?php else: ?>
	<article class="col-md-12">
<?php endif; ?>

	<?php
	global $post;
	$this_slug = $post->ID;
	// echo the POST ID HERE FOR TESTING
	//echo '<h1> The POST ID: '. $this_slug . '</h1>';
	?>
    <?php if( 35 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'oslobtumalogkawasan' ); ?>
    <?php elseif( 38 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'oslobsumilonkawasan' ); ?>
    <?php elseif( 88 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'oslobtumalogsumilon' ); ?>
    <?php elseif( 91 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'oslobtumalogsimala' ); ?>
    <?php elseif( 97 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'oslobovernightbudget' ); ?>
    <?php elseif( 100 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'oslobovernightfull' ); ?>
    <?php elseif( 94 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', '3d2ncebubohol' ); ?>
    <?php elseif( 130 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', '4d3ncebu' ); ?>
    <?php elseif( 133 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', '5d4ncebubohol' ); ?>
    <?php elseif( 136 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', '6d5ncebubohol' ); ?>
    <?php elseif( 176 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'malapascuakalanggaman' ); ?>
    <?php elseif( 179 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'malapascua' ); ?>
    <?php elseif( 182 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'kalanggaman' ); ?>
    <?php elseif( 185 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'mactanisland' ); ?>
    <?php elseif( 188 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'cebucity' ); ?>
    <?php elseif( 191 == $this_slug) : ?>
    	<?php get_template_part( 'parts/template-oslobtours', 'pescadorcanyoneering' ); ?>

    <?php else: ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div <?php post_class(); ?>>
					<div class="single-content">
						<div class="single-entry-summary">
							<?php the_content(); ?>
						</div><!-- .single-entry-summary -->
						<?php wp_link_pages(); ?>
					</div>
					<div class="single-footer row">
						<div class="col-md-4">
							<?php get_template_part( 'template-parts/template-part', 'postauthor' ); ?>
						</div>
						<div class="col-md-8">
							<?php comments_template(); ?>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>


	<?php endif; ?>
	</article>

<?php
if (!in_array($this_ID, $postTours)) {

	get_sidebar( 'right' );

}
?>
</div>
<!-- end content container -->




<?php get_footer(); ?>
