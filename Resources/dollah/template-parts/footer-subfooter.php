<?php
if ( ! is_active_sidebar( 'dollah-sub-footer' ) )
	return;
?>
<div <?php hybridextend_attr( 'sub-footer', '', 'hgrid-stretch inline_nav' ); ?>>
	<div class="hgrid">
		<div class="hgrid-span-12">
			<?php dynamic_sidebar( 'dollah-sub-footer' ); ?>
		</div>
	</div>
</div>